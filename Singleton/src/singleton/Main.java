package singleton;

public class Main {
	public static void main (String ARG[]) {
		int x = 2;
		int y = 2;
		Operacion c = Operacion.getInstancia();
		c.Sumar(x,y);
		c.Restar(x,y);
		
		boolean rpta = c instanceof Operacion;     //ver retorno del objeto clase conexion por un boolean
		System.out.println(rpta);
		
		
	}
}
