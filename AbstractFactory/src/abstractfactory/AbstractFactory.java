package abstractfactory;
import abstractfactory.FabricaAbstracta;
import abstractfactory.FabricaElfos;
import abstractfactory.FabricaHumanos;
import abstractfactory.FabricaOrcos;
import abstractfactory.Arma;
import abstractfactory.Cuerpo;
import abstractfactory.Escudo;
import java.util.Scanner;

public class AbstractFactory implements Ejemplo {
        FabricaAbstracta fabrica;
        Arma arma;
        Cuerpo cuerpo;
        Escudo escudo; 
        Scanner escaner;
    String Opcion;

   

	public void operacion(String opcion) {
		
		Opcion = opcion;
        switch (Opcion) {
            case "Humano":
                fabrica = new FabricaHumanos();
                break;
            case "Elfo":
                fabrica = new FabricaElfos();
                break;
            case "Orco":
                fabrica = new FabricaOrcos();
                break;    
        }

        arma = fabrica.crearArma();
        cuerpo = fabrica.crearCuerpo();
        escudo = fabrica.crearEscudo();

        System.out.println(arma.operacion());
        System.out.println(cuerpo.operacion());
        System.out.println(escudo.operacion());

    }



	@Override
	public void operacion() {
		// TODO Auto-generated method stub
		
	}
}

    

