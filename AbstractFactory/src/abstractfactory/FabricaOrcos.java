
package abstractfactory;

public class FabricaOrcos implements FabricaAbstracta{

    @Override
    public Arma crearArma() {
        return new ArmaOrco();
    }

    @Override
    public Cuerpo crearCuerpo() {
        return new CuerpoOrco();
    }

    @Override
    public Escudo crearEscudo() {
        return new EscudoOrco();
    }
    
}
