
package abstractfactory;
import abstractfactory.Arma;
import abstractfactory.Cuerpo;
import abstractfactory.Escudo;

public interface FabricaAbstracta {
    
    public Arma crearArma();

    public Cuerpo crearCuerpo();

    public Escudo crearEscudo();
}
