package Interfaz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import abstractfactory.AbstractFactory;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollBar;


public class Ventana1 extends JFrame {

	private JPanel contentPane;
	JLabel LabelOpcion;
	JComboBox comboBoxP;
	String Opcion;
	private JLabel labelPersonaje;
	private JLabel labelArma;
	private JLabel labelEscudo;
	
	public Ventana1() {
		setTitle("AbstractFactory");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 595, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		LabelOpcion = new JLabel("Personaje:");
		LabelOpcion.setBounds(10, 11, 73, 14);
		contentPane.add(LabelOpcion);
		
		
		labelPersonaje = new JLabel("");						//label para implementacion de imagenes
		labelPersonaje.setBounds(175, 36, 261, 229);
		contentPane.add(labelPersonaje);
		labelPersonaje.setVisible(false);
		
		labelArma = new JLabel("");
		labelArma.setBounds(446, 79, 120, 120);
		contentPane.add(labelArma);
		labelArma.setVisible(false);
		
		labelEscudo = new JLabel("");
		labelEscudo.setBounds(446, 210, 120, 120);
		contentPane.add(labelEscudo);
		labelEscudo.setVisible(false);							//
		
		comboBoxP = new JComboBox();
		comboBoxP.setModel(new DefaultComboBoxModel(new String[] {"-", "Humano", "Elfo", "Orco"}));
		
		comboBoxP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				labelPersonaje.setVisible(false);
				labelArma.setVisible(false);
				labelEscudo.setVisible(false);
				
				Opcion = (String) comboBoxP.getSelectedItem();
				AbstractFactory snd = new AbstractFactory();
				
				switch (Opcion) {
					case "-":
						
						JOptionPane.showMessageDialog(getContentPane(), "Seleccion algun personaje",
								"Error", JOptionPane.ERROR_MESSAGE);
						;break;
					case "Humano":
						
						JOptionPane.showMessageDialog(getContentPane(), "Humano seleccionado",
								"Aviso", JOptionPane.INFORMATION_MESSAGE);
						
						
						snd.operacion(Opcion);					//Cada vez que se envie Opcion atravez de objeto, se debe cargar imagenes del
						labelPersonaje.setVisible(true);		//personaje seleccionado
						labelArma.setVisible(true);
						labelEscudo.setVisible(true);
						
						
						;break;
					case "Elfo":
						
						JOptionPane.showMessageDialog(getContentPane(), "Elfo seleccionado",
								"Aviso", JOptionPane.INFORMATION_MESSAGE);
						
						snd.operacion(Opcion);					//Cada vez que se envie Opcion atravez de objeto, se debe cargar imagenes del
						labelPersonaje.setVisible(true);		//personaje seleccionado
						labelPersonaje.setVisible(true);
						labelArma.setVisible(true);
						labelEscudo.setVisible(true);
						
						
						;break;
					case "Orco":
						
						JOptionPane.showMessageDialog(getContentPane(), "Orco seleccionado",
								"Aviso", JOptionPane.INFORMATION_MESSAGE);
						
						snd.operacion(Opcion);					//Cada vez que se envie Opcion atravez de objeto, se debe cargar imagenes del
						labelPersonaje.setVisible(true);		//personaje seleccionado
						labelPersonaje.setVisible(true);
						labelArma.setVisible(true);
						labelEscudo.setVisible(true);
						
						
						;break;	
				}
				
			}
		});

		comboBoxP.setToolTipText("");
		comboBoxP.setBounds(10, 36, 93, 20);
		contentPane.add(comboBoxP);
		
	
		
		
		
	}
}
